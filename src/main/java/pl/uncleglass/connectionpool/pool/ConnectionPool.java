package pl.uncleglass.connectionpool.pool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class ConnectionPool {
    private final String url;
    private final String user;
    private final String password;
    private final List<Connection> idleConnections;
    private final List<Connection> usedConnections = new ArrayList<>();
    private static final int INITIAL_POOL_SIZE = 10;
    private static final int MAX_POOL_SIZE = 50;
    private static final AtomicInteger activeConnectionCounter = new AtomicInteger(0);

    public static ConnectionPool create(String url, String user, String password) throws SQLException {
        List<Connection> idleConnections = new ArrayList<>(INITIAL_POOL_SIZE);
        for (var i = 0; i < INITIAL_POOL_SIZE; i++) {
            idleConnections.add(createConnection(url, user, password));
        }
        return new ConnectionPool(url, user, password, idleConnections);
    }

    private ConnectionPool(String url, String user, String password, List<Connection> idleConnections) {
        this.url = url;
        this.user = user;
        this.password = password;
        this.idleConnections = idleConnections;
    }

    private static Connection createConnection(String url, String user, String password) throws SQLException {
        return DriverManager.getConnection(url, user, password);
    }

    public synchronized Connection getConnection() throws SQLException {
        if (isMaxPoolSizeReached()) {
            return null;
        }
        if (isNoAvailableConnections()) {
            addAdditionalConnection();
        }
        return shareConnection();
    }

    private boolean isMaxPoolSizeReached() {
        return activeConnectionsCount() == MAX_POOL_SIZE;
    }

    private int activeConnectionsCount() {
        return activeConnectionCounter.get();
    }

    private boolean isNoAvailableConnections() {
        return idleConnections.isEmpty();
    }

    private void addAdditionalConnection() throws SQLException {
        var connection = createConnection(url, user, password);
        idleConnections.add(connection);
    }

    private Connection shareConnection() {
        var connection = idleConnections.remove(indexOfLastIdle());
        usedConnections.add(connection);
        activeConnectionCounter.incrementAndGet();
        return connection;
    }

    private int indexOfLastIdle() {
        return idleSize() - 1;
    }

    private int idleSize() {
        return idleConnections.size();
    }

    public synchronized void releaseConnection(Connection connection) throws SQLException {
        usedConnections.remove(connection);
        activeConnectionCounter.decrementAndGet();
        if (connection.isValid(1)) {
            idleConnections.add(connection);
        } else {
            addAdditionalConnection();
        }
        if (isDemandForConnectionsLess() || isNoActiveConnections()) {
            removeIdleExcess();
        }
    }

    private boolean isDemandForConnectionsLess() {
        return idleSize() > INITIAL_POOL_SIZE && activeConnectionsCount() < INITIAL_POOL_SIZE;
    }

    private boolean isNoActiveConnections() {
        return activeConnectionsCount() == 0;
    }

    private void removeIdleExcess() throws SQLException {
        for (int i = indexOfLastIdle(); i >= INITIAL_POOL_SIZE; i--) {
            var connection = idleConnections.remove(i);
            connection.close();
        }
    }
}
