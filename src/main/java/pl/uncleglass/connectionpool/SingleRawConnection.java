package pl.uncleglass.connectionpool;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.Duration;
import java.time.Instant;

public class SingleRawConnection {
    private static final String URL = "jdbc:postgresql://localhost:5432/pool";
    private static final String USER = "user";
    private static final String PASSWORD = "password";
    private static final String SQL = "INSERT INTO testtable(testcolumn) VALUES('test')";

    public static void main(String[] args) {
        var start = Instant.now();
        try (var connection = DriverManager.getConnection(URL, USER, PASSWORD);
             var statement = connection.createStatement()) {
            for (int i = 0; i < 10_000; i++) {
                statement.execute(SQL);
            }
        } catch (SQLException e) {
        }


        var end = Instant.now();
        var duration = Duration.between(start, end);
        System.out.println(duration.getSeconds());
        //test #1 21 seconds
        //test #2 22 seconds
        //test #3 21 seconds
        //test #4 22 seconds
        //test #5 21 seconds
    }
}
