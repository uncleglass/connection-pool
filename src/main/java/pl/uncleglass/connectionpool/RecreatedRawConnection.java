package pl.uncleglass.connectionpool;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.Duration;
import java.time.Instant;

public class RecreatedRawConnection {
    private static final String URL = "jdbc:postgresql://localhost:5432/pool";
    private static final String USER = "user";
    private static final String PASSWORD = "password";
    private static final String SQL = "INSERT INTO testtable(testcolumn) VALUES('test')";

    public static void main(String[] args) {
        var start = Instant.now();
        for (int i = 0; i < 10_000; i++) {
            try (var connection = DriverManager.getConnection(URL, USER, PASSWORD);
                 var statement = connection.createStatement()) {
                statement.execute(SQL);

            } catch (SQLException e) {

            }
        }
        var end = Instant.now();
        var duration = Duration.between(start, end);
        System.out.println(duration.getSeconds());
        //test #1 111 seconds
        //test #2 114 seconds
        //test #3 116 seconds
        //test #4 114 seconds
        //test #5 107 seconds
    }
}
