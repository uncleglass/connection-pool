package pl.uncleglass.connectionpool;

import pl.uncleglass.connectionpool.pool.ConnectionPool;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class UsingConnectionPool {
    private static final String URL = "jdbc:postgresql://localhost:5432/pool";
    private static final String USER = "user";
    private static final String PASSWORD = "password";
    private static final String SQL = "INSERT INTO testtable(testcolumn) VALUES('test')";

    public static void main(String[] args) throws Exception {
        ConnectionPool connectionPool = ConnectionPool.create(URL, USER, PASSWORD);
        ExecutorService executorService = Executors.newFixedThreadPool(60);
        var start = Instant.now();
        for (var i = 0; i < 10000; i++) {
            executorService.execute(() -> {
                    try {
                        Connection connection;
                        do {
                            connection = connectionPool.getConnection();
                        } while (connection == null);
                        var statement = connection.createStatement();
                        statement.execute(SQL);
                        statement.close();
                        connectionPool.releaseConnection(connection);
                    } catch (SQLException e) {
                        System.out.println(e.getMessage());
                    }
            });

        }

        var end = Instant.now();
        executorService.shutdown();
        executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        var duration = Duration.between(start, end);
        System.out.println("Duration: " +duration.toMillis());
        //test #1 364 milliseconds
        //test #2 348 milliseconds
        //test #3 346 milliseconds
        //test #4 361 milliseconds
        //test #5 284 milliseconds
    }
}
